import json
import shutil
import tarfile

def fsbackup(files_to_backup, bkp_path):
    try:
        shutil.copytree(files_to_backup, bkp_path)
    # Directories are the same
    except shutil.Error as e:
        print('Directory not copied. Error: %s' % e)
    # Any error saying that the directory doesn't exist
    except OSError as e:
        print('Directory not copied. Error: %s' % e)()


if __name__ == '__main__':
    with open('fs.json') as config_file:
        data = json.load(config_file)
    
    fs_backup = data['filesystem']
    bkp_path = data['backup_path']
    PATH = str(input("Path: "))
    fsbackup(PATH, bkp_path)


    #print(fs_backup)
    #print(bkp_path)    
